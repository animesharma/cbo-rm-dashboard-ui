import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from './layout/default/default.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { PostsComponent } from './modules/posts/posts.component';


const routes: Routes = [{
  path:'',
  component: DefaultComponent,
  children: [{
    path:'',
    redirectTo: '/cloudEngineering', pathMatch : 'full'
  },{
    path:'cloudEngineering',
    component: DashboardComponent,
    data :{category:'Cloud Engineering'}
  },{
    path:'cloudMigrationATDATA',
    component: PostsComponent,
    data :{category:'Cloud Migration & ATADATA'}
  },{
    path:'cloudManagedServices',
    component: PostsComponent,
    data :{category:'Cloud Managed Services'}
  },{
    path:'cloudInfraEngg',
    component: PostsComponent,
    data :{category:'Cloud Infrastructure & Engineering'}
  },{
    path:'cloudStrategyOpModel',
    component: PostsComponent,
    data :{category:'Cloud Strategy & Op Model '}
  },{
    path:'cloudIntegrationAPIs',
    component: PostsComponent,
    data :{category:'Cloud Integration & APIs'}
  },{
    path:'cloudDevelopmentIntegration',
    component: PostsComponent,
    data :{category:'Cloud Development & Integration'}
  },{
    path:'opsTransformation',
    component: DashboardComponent,
    data :{category:'Operations Transformation'}
  },{
    path:'opsModelTransformation',
    component: PostsComponent,
    data :{category:'Op Model Transformation'}
  },{
    path:'opsModelTransformationHealth',
    component: PostsComponent,
    data :{category:'Op Model Transformation - Health Care'}
  },{
    path:'revenueCycleTransformation',
    component: PostsComponent,
    data :{category:'Revenue Cycle Transformation'}
  },{
    path:'techOperations',
    component: PostsComponent,
    data :{category:'Tech Operations'}
  },{
    path:'coreIndustrySolutions',
    component: DashboardComponent,
    data :{category:'Core Industry Solutions'}
  },{
    path:'convergeConsulting',
    component: PostsComponent,
    data :{category:'Converge Consulting'}
  },{
    path:'digitalBankingSolutions',
    component: PostsComponent,
    data :{category:'Digital Banking Solutions'}
  },{
    path:'digitalCare',
    component: PostsComponent,
    data :{category:'Digital CARE'}
  },{
    path:'emergingIndustrySolutions',
    component: PostsComponent,
    data :{category:'Emerging Industry & Solutions'}
  },{
    path:'insuranceCST',
    component: PostsComponent,
    data :{category:'Insurance CST'}
  },{
    path:'systemEngg',
    component: DashboardComponent,
    data :{category:'Systems Engineering'}
  },{
    path:'appArchitecture',
    component: PostsComponent,
    data :{category:'Application Architecture'}
  },{
    path:'appModernization',
    component: PostsComponent,
    data :{category:'Application Modernization'}
  },{
    path:'integrationServices',
    component: PostsComponent,
    data :{category:'Integration Services'}
  },{
    path:'serviceDeliveryOptimization',
    component: PostsComponent,
    data :{category:'Service Delivery Optimization'}
  },{
    path:'systemDesignsEngg',
    component: PostsComponent,
    data :{category:'Systems Design & Engineering'}
  }]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
