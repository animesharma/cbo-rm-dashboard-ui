import { GraphModel } from './graph-model';

export interface GraphData{
    xaxisData: string[];
    yaxisData: GraphModel[];
}