export interface GraphModel {
    name : string;
    data : number[];
}

export class GraphModelImpl implements GraphModel{
    
    public set name(value:string) {
        this.name=value;
    }

    public set data (value: number[]){
        this.data=value;
    }
}