export interface TabData {
  category: string;
  account: string;
  requestType: string;
  sourceType: string;
  probability: string;
  p1: number;
  p2: number;
  p3: number;
  p4: number;
  p5: number;
  p6: number;
  p7: number;
  p8: number;
  p9: number;
  p10: number;
  p11: number;
  p12: number;
  p13: number;
  skill1:string;
  skill2:string;
  skill3:string;
  skill4:string;
}
