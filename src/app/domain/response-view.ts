import { ValuesModel } from './values-model';

export interface ResponseView {
    period: string;
    category: string;
    values: ValuesModel[] ;
    resignations: number;
    availability: number;
}