export interface ValuesModel {
    value: number;
    category: string;
}