export interface SingleUploadFormModel {
    fileType:File;
    filename:string;
    // CloudEngineering: string;
    // OperationsTransformation: string;
    // CoreIndustrySolutions: string;
    // SystemsEngineering: string;
    opt1: boolean;
    opt2: boolean;
    opt3: boolean;
    opt4: boolean;
    financialYear: string;
    periods: string;
}