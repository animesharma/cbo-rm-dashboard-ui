import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TabData } from 'src/app/domain/tab-data';
import { ResponseView } from 'src/app/domain/response-view';
import { GraphModel, GraphModelImpl } from 'src/app/domain/graph-model';
import { GraphData } from 'src/app/domain/graph-data';

@Injectable()
export class DashboardServiceService {

  private _jsonURL = 'assets/tabData.json';

  constructor(private http: HttpClient) {}

  getData(url: string) : Promise<any> {
    console.log("Endpoint called - "+url);
    return this.http.get(url)
                    .toPromise()
                    .then((res: any) => <TabData[]> res.data)
                    .then(data => { return data; });
  }

  getGraphData(url: string) : Promise<GraphData> {
    console.log("Endpoint called - "+url);
    return this.http.get(url)
                    .toPromise()
                    .then((res: any) => this.extractGraphData(<ResponseView[]> res.data));
                    // .then(data => {  return data; });//console.log("Graph Data - "+JSON.stringify(data));
    }

  extractGraphData(responseView: ResponseView[]) {
    //console.log("Graph Data - "+JSON.stringify(responseView));
    let xaxisData: string[] = [];
    let yaxisData: GraphModel[] = [];
    let rollOffForecast: string = null;
    let rollOffActual: string = null;
    let graphModel1: GraphModelImpl={name : "", data: []};
    let graphModel2: GraphModelImpl={name : "", data: []};
    let graphData: GraphData={xaxisData:[],yaxisData:[]};
    responseView.forEach(function (value) {
      //console.log(value.period);
      xaxisData.push(value.period) ;
      let i = 0;
      value.values.forEach(function (val){
       //console.log("Value -["+val.category+","+val.value+"]");
       i=i+1;
        if(i==1){
          if(graphModel1.name===""){
            graphModel1.name=  val.category;
          }
        }else if(i==2){
          if(graphModel2.name===""){
            graphModel2.name= val.category;
          }
        }
        if(graphModel1.name===val.category){
          graphModel1.data.push(val.value);
        } else if (graphModel2.name===val.category){
          graphModel2.data.push(val.value);
        }
      });
    }); 
    if(graphModel1.name!=""){
     yaxisData.push(graphModel1);
    }
    if(graphModel2.name!=""){
    console.log("GRAPHMODEL2-",graphModel2);
     yaxisData.push(graphModel2);
    }
    //set in object
    //return the object
    graphData.xaxisData = xaxisData;
    graphData.yaxisData = yaxisData;
    console.log("Final Data - "+JSON.stringify(graphData));
    return graphData;
  }
}
