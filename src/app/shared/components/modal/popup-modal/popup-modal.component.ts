import { Component, OnInit, Inject, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SingleUploadFormModel } from 'src/app/domain/singleUploadFormModel';

export interface DialogData {
  isUpload : boolean;
}

@Component({
  selector: 'app-popup-modal',
  templateUrl: './popup-modal.component.html',
  styleUrls: ['./popup-modal.component.scss']
})
export class PopupModalComponent implements OnInit {

  singleUploadForm: FormGroup;
  
  //@Input() isUpload: boolean;

    constructor(private fb: FormBuilder,public dialogRef: MatDialogRef<PopupModalComponent>,
      @Inject(MAT_DIALOG_DATA) {fileType,filename, financialYear,periods,opt1,opt2,opt3,opt4}:SingleUploadFormModel, @Inject(MAT_DIALOG_DATA) public data: DialogData) {

        this.singleUploadForm = fb.group({
          fileType: [fileType, Validators.required],
          filename: [filename, Validators.required],
          opt1:false,
          opt2:false,
          opt3:false,
          opt4:false,
          financialYear: [financialYear, Validators.required],
          periods: [periods,Validators.required]
      });

      }

  ngOnInit() {
    
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  
}
