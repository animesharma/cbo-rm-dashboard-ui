import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-popup-download-modal',
  templateUrl: './popup-download-modal.component.html',
  styleUrls: ['./popup-download-modal.component.scss']
})
export class PopupDownloadModalComponent implements OnInit {

  downloadCSV: FormGroup;
  
  constructor(private fb: FormBuilder, @Inject(MAT_DIALOG_DATA) offering) {
    this.downloadCSV = fb.group({
      offering:offering
    });
  }

  ngOnInit() {
  }

}
