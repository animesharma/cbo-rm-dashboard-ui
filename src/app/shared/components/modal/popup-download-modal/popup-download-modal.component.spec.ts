import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupDownloadModalComponent } from './popup-download-modal.component';

describe('PopupDownloadModalComponent', () => {
  let component: PopupDownloadModalComponent;
  let fixture: ComponentFixture<PopupDownloadModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupDownloadModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupDownloadModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
