import { Component, OnInit, Output,EventEmitter, Inject } from '@angular/core';
import {MatDialog, MatDialogConfig} from "@angular/material";
import { PopupModalComponent } from '../modal/popup-modal/popup-modal.component';
import { PopupDownloadModalComponent } from '../modal/popup-download-modal/popup-download-modal.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  email: string;

  @Output() toggleSideBarField: EventEmitter<any> = new EventEmitter();
  constructor(private dialog: MatDialog) { }

  ngOnInit() {}

  toggleSideBar(){
    this.toggleSideBarField.emit();
    setTimeout(() => {
      window.dispatchEvent(
        new Event('resize')
      );
    },300);
  }

  openUploadDialog(id:string): void {
    console.log("Btn ID :"+id);
    let isUpload = true;
    if(id==='downloadBtn'){
      isUpload=false;
    }
    const dialogRef = this.dialog.open(PopupModalComponent, {
      width: '700px',
      height: '500px',
      data: {isUpload: isUpload}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.email = result;
    });
  }

  openDownloadReportDialog(id:string): void {
    console.log("Btn ID :"+id);
    let isUpload = true;
    if(id==='downloadBtn'){
      isUpload=false;
    }
    const dialogRef = this.dialog.open(PopupDownloadModalComponent, {
      width: '500px',
      height: '250px',
      data: {isUpload: isUpload}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.email = result;
    });
  }
}
