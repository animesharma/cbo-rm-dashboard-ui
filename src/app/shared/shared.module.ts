import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { MatDividerModule, MatToolbarModule, MatIconModule, MatButtonModule, MatMenuModule, MatListModule, MatPaginatorModule, MatFormFieldModule, MatInputModule, MatSortModule, MatDialogModule, MatCardModule, MatSelectModule, MatCheckboxModule } from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import { RouterModule } from '@angular/router';
import { BasicColumnComponent } from './widgets/charts/basic-column/basic-column.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { CardComponent } from './widgets/card/card.component';
import {MatTableModule} from '@angular/material/table';
import { DynamicTableComponent } from './widgets/table/dynamic-table/dynamic-table.component';
import { DynamicNgTableComponent } from './widgets/table/dynamic-ng-table/dynamic-ng-table.component';
import { TableModule } from 'primeng/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { DynamicColEditTableComponent } from './widgets/table/dynamic-col-edit-table/dynamic-col-edit-table.component';
import { PopupModalComponent } from './components/modal/popup-modal/popup-modal.component';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { PopupDownloadModalComponent } from './components/modal/popup-download-modal/popup-download-modal.component';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    BasicColumnComponent,
    CardComponent,
    DynamicTableComponent,
    DynamicNgTableComponent,
    DynamicColEditTableComponent,
    PopupModalComponent,
    PopupDownloadModalComponent
  ],
  imports: [
    CommonModule,
    MatDividerModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    FlexLayoutModule,
    MatMenuModule,
    MatListModule,
    RouterModule,
    HighchartsChartModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSortModule,
    TableModule,
    FormsModule,
    ButtonModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatCardModule,
    MatSelectModule,
    MatCheckboxModule,
    MaterialFileInputModule,
    ChartsModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    BasicColumnComponent,
    CardComponent,
    DynamicTableComponent,
    DynamicNgTableComponent,
    DynamicColEditTableComponent,

  ],
  entryComponents: [PopupModalComponent,
    PopupDownloadModalComponent]
})
export class SharedModule { }
