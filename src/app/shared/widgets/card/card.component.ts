import { Component, OnInit, Input } from '@angular/core';
import * as Highcharts from 'highcharts';
import HC_exporting from 'highcharts/modules/exporting';
import { DashboardServiceService } from 'src/app/core/service/dashboard-service.service';
import { ResponseView } from 'src/app/domain/response-view';
import { GraphData } from 'src/app/domain/graph-data';
import { GraphModel } from 'src/app/domain/graph-model';
import { Color } from 'ng2-charts';

@Component({
  selector: 'app-widget-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  
  @Input() data:GraphModel[] = [];

  public barChartOptions = {
    scaleShowVerticalLines:false,
    responsive:true
  }
  public barChartLabels=['P1','P2','P3','P4','P5','P6','P7','P8','P9','P10','P11','P12','P13'];

  public barChartType='bar';
  public barChartLegend=true;
  public barChartData=[
    {label:'',data:[]}
  ];
  public barChartColors: Color[] = [
    { backgroundColor: '#009933' },
    { backgroundColor: '#3392FF' },
  ]

  constructor() { }

  ngOnChanges(){
    // console.log("DATA for Graph ONCHANGE- ",this.data);
    if(this.data === undefined){
      console.log("Data - UNDEFINED");
    }else{
      this.barChartData[0].label=this.data[0].name;  
      this.barChartData[0].data=this.data[0].data;  
      if(this.data[1]===undefined){
          console.log("DATA[1] not present");
      }else{
        console.log("DATA & LABEL ADDED FOR DATA[1]");
        this.barChartData.push({label:this.data[1].name,data:this.data[1].data});
        this.barChartColors[1].backgroundColor="blue";
      }
      // this.barChartData[1].label=this.data[1].name;  
      // this.barChartData[1].data=this.data[1].data;  
    }
  }

  ngOnInit() {
    // console.log("DATA for Graph INIT- "+this.data); 
  //   HC_exporting(Highcharts);
    setTimeout(() => {
      window.dispatchEvent(
        new Event('resize')
      );
    },300);
  }

}
