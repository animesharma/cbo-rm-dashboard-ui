import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import HC_exporting from 'highcharts/modules/exporting';

@Component({
  selector: 'app-widget-chart-basic-column',
  templateUrl: './basic-column.component.html',
  styleUrls: ['./basic-column.component.scss']
})
export class BasicColumnComponent implements OnInit {

  chartOptions: {};
  Highcharts = Highcharts;
  constructor() { }

  ngOnInit() {
    this.chartOptions = {
      chart: {
          type: 'column'
      },
      title: {
          text: 'Roll-off Data'
      },
      subtitle: {
          text: 'Source: Deloitte.com'
      },
      xAxis: {
          categories: [
            'P1',
            'P2',
            'P3',
            'P4',
            'P5',
            'P6',
            'P7',
            'P8',
            'P9',
            'P10',
            'P11',
            'P12',
            'P13'
          ],
          crosshair: true
      },
      yAxis: {
          min: 0,
          title: {
              text: 'Roll-offs'
          }
      },
      tooltip: {
          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
          footerFormat: '</table>',
          shared: true,
          useHTML: true
      },
      plotOptions: {
          column: {
              pointPadding: 0.2,
              borderWidth: 0
          }
      },
      exporting: {
        enabled: true,
      },
      credits: {
        enabled: false
      },
      series: [{
        name: 'Roll-Off Forecast',
        data: [9, 7, 10, 12, 14, 17, 35, 48, 21, 19, 40, 54]

    }, {
        name: 'Roll-Off Actuals',
        data: [3, 7, 9, 9, 14, 17, 35, 40, 20, 15, 39, 52]

    }]
  };
  HC_exporting(Highcharts);
  setTimeout(() => {
    window.dispatchEvent(
      new Event('resize')
    );
  },300);
  }
  
}
