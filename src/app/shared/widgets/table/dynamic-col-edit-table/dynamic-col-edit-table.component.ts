import { Component, OnInit, Input, ViewChild } from '@angular/core';

@Component({
  selector: 'app-dynamic-col-edit-table',
  templateUrl: './dynamic-col-edit-table.component.html',
  styleUrls: ['./dynamic-col-edit-table.component.scss']
})
export class DynamicColEditTableComponent implements OnInit {
  @Input() tableData : String;
  @Input() columnHeaders : String;
  @Input() tableCaption : String;
  @Input() actionHeader : String;
  @Input() isAction : boolean;
  columnHeaderVal : number;
  headerArray : String[] = ["category","account","requestType","sourceType","probability"];

  clonedCars: { [s: string]: any; } = {};

  constructor() { }

  ngOnInit() { }

  isEditable(columnHeader) {
    //console.log("Col Index - "+columnHeader);
    if(!'category'.match(columnHeader)){
      this.columnHeaderVal = +columnHeader.slice(1,columnHeader.length);
      if(this.isFuturePeriod(this.columnHeaderVal))
        return true;
      else
        return false;
    }else {
      return false;
    }
  }

  getBgColor(columnHeader){
    //console.log("column Header -"+ columnHeader);
    if(!this.headerArray.some(e => e == columnHeader)){
      this.columnHeaderVal = +columnHeader.slice(1,columnHeader.length);
      if(this.isFuturePeriod(this.columnHeaderVal))
        return "future-period";
      else
        return "old-period";
    }else if('action'.match(columnHeader) || 'comment'.match(columnHeader)){
      return null;
    }
    return "sticky-header";
  }

  isFuturePeriod(period:number){
    // console.log(period>=this.calculateCurrentPeriod());
   if(period>=this.calculateCurrentPeriod())
   return true;
  }

  calculateCurrentPeriod(){
    //add logic to calculate current period
    return 9;
  }

  getColWidth(columnHeader){
    // console.log(columnHeader);
    if(columnHeader.length >6)
        return "15%";
    else if (columnHeader.length >3)
        return "10%";
    else
        return "5%";
  }

  onRowEditInit(car: any) {
    // this.clonedCars[car.category] = {...car};
    console.log("onRowEditInit - Data - "+JSON.stringify(car));
    console.log("onRowEditInit - Data - "+JSON.stringify(this.clonedCars));
  }

  onRowEditSave(car: any) {
      // if (car.year > 0) {
      //     delete this.clonedCars[car.vin];
      //     this.messageService.add({severity:'success', summary: 'Success', detail:'Car is updated'});
      // }
      // else {
      //     this.messageService.add({severity:'error', summary: 'Error', detail:'Year is required'});
      // }
      console.log("onRowEditSave - Data - "+JSON.stringify(car));
  }

  onRowEditCancel(car: any, index: number) {
      // this.cars2[index] = this.clonedCars[car.vin];
      // delete this.clonedCars[car.vin];
      console.log("onRowEditCancel - RI : "+JSON.stringify(index)+" | Data - "+JSON.stringify(car));
  }

}
