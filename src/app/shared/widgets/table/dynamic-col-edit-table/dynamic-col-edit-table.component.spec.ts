import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicColEditTableComponent } from './dynamic-col-edit-table.component';

describe('DynamicColEditTableComponent', () => {
  let component: DynamicColEditTableComponent;
  let fixture: ComponentFixture<DynamicColEditTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicColEditTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicColEditTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
