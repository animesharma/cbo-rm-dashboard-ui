import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatTableDataSource, MatPaginator} from '@angular/material';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-dynamic-table',
  templateUrl: './dynamic-table.component.html',
  styleUrls: ['./dynamic-table.component.scss']
})

export class DynamicTableComponent implements OnInit {

  @Input() tableData;
  @Input() columnHeader;
  @Input() filterEnabled : boolean;
  currentPeriod : string;

  objectKeys = Object.keys;
  dataSource;
  columnHeaderVal : number;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {
    console.log(this.tableData);
    this.dataSource = new MatTableDataSource(this.tableData);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getBgColor(columnHeader){
    // console.log(columnHeader);
    if(!'Category'.match(columnHeader)){
      this.columnHeaderVal = +columnHeader.slice(1,columnHeader.length);
      if(this.isFuturePeriod(this.columnHeaderVal))
        return "3392FF";
      else
        return "92DA3A";
    }else if('Action'.match(columnHeader) || 'Save'.match(columnHeader)){
      return "ffffff";
    }
    return "009933";
  }

  isFuturePeriod(period:number){
    // console.log(period>=this.calculateCurrentPeriod());
   if(period>=this.calculateCurrentPeriod())
   return true;
  }

  calculateCurrentPeriod(){
    //add logic to calculate current period
    return 10;
  }

  getColWidth(columnHeader){
    // console.log(columnHeader);
    if(columnHeader.length >6)
        return "15%";
    else if (columnHeader.length >3)
        return "6%";
    else
        return "5%";
  }
  
  isButton(columnHeader:string){
    if("action" === columnHeader.toLowerCase() || "save" === columnHeader.toLowerCase())
     return true;
  }
}
 


