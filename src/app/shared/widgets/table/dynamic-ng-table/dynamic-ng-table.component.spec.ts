import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicNgTableComponent } from './dynamic-ng-table.component';

describe('DynamicNgTableComponent', () => {
  let component: DynamicNgTableComponent;
  let fixture: ComponentFixture<DynamicNgTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicNgTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicNgTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
