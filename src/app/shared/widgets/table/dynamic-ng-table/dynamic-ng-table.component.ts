import { Component, OnInit, Input, ViewChild } from '@angular/core';

@Component({
  selector: 'app-dynamic-ng-table',
  templateUrl: './dynamic-ng-table.component.html',
  styleUrls: ['./dynamic-ng-table.component.scss']
})
export class DynamicNgTableComponent implements OnInit {

  @Input() tableData;
  @Input() columnHeaders;
  @Input() tableCaption;
  @Input() actionHeader;
  @Input() isAction;
  columnHeaderVal : number;

  clonedCars: { [s: string]: any; } = {};

  constructor() { }

  ngOnInit() {
  }

  onRowEditInit(car: any) {
    // this.clonedCars[car.category] = {...car};
    console.log("onRowEditInit - Data - "+JSON.stringify(car));
    console.log("onRowEditInit - Data - "+JSON.stringify(this.clonedCars));
  }

  isEditable(columnHeader) {
    //console.log("Col Index - "+columnHeader);
    if(!'category'.match(columnHeader) || 'Action'.match(columnHeader) || 'Save'.match(columnHeader)){
      this.columnHeaderVal = +columnHeader.slice(1,columnHeader.length);
      if(this.isFuturePeriod(this.columnHeaderVal))
        return true;
      else
        return false;
    }else {
      return false;
    }
  }

  getBgColor(columnHeader){
    //console.log(columnHeader);
    if(!(0=='category'.localeCompare( columnHeader))){
      this.columnHeaderVal = +columnHeader.slice(1,columnHeader.length);
      if(this.isFuturePeriod(this.columnHeaderVal))
        return "future-period";
      else
        return "old-period";
    }else if('Action'.match(columnHeader) || 'Save'.match(columnHeader)){
      return null;
    }
    return "sticky-header";
  }

  isFuturePeriod(period:number){
    // console.log(period>=this.calculateCurrentPeriod());
   if(period>=this.calculateCurrentPeriod())
   return true;
  }

  calculateCurrentPeriod(){
    //add logic to calculate current period
    return 9;
  }

  getColWidth(columnHeader){
    // console.log(columnHeader);
    if(columnHeader.length >6)
        return "10%";
    else if (columnHeader.length >3)
        return "10%";
    else
        return "5%";
  }

  onRowEditSave(car: any) {
      // if (car.year > 0) {
      //     delete this.clonedCars[car.vin];
      //     this.messageService.add({severity:'success', summary: 'Success', detail:'Car is updated'});
      // }
      // else {
      //     this.messageService.add({severity:'error', summary: 'Error', detail:'Year is required'});
      // }
      console.log("onRowEditSave - Data - "+JSON.stringify(car));
  }

  onRowEditCancel(car: any, index: number) {
      // this.cars2[index] = this.clonedCars[car.vin];
      // delete this.clonedCars[car.vin];
      console.log("onRowEditCancel - RI : "+JSON.stringify(index)+" | Data - "+JSON.stringify(car));
  }
}
