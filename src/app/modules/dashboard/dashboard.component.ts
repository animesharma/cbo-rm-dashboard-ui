import { Component, OnInit } from '@angular/core';
import { DashboardServiceService } from 'src/app/core/service/dashboard-service.service';
import { TabData } from 'src/app/domain/tab-data';
import { GraphData } from 'src/app/domain/graph-data';
import { GraphModel } from 'src/app/domain/graph-model';
import { environment } from './../../../environments/environment';
import { ActivatedRoute } from '@angular/router';

export interface TableElement {
  category: string;
  p1: number;
  p2: number;
  p3: number;
  p4: number;
  p5: number;
  p6: number;
  p7: number;
  p8: number;
  p9: number;
  p10: number;
  p11: number;
  p12: number;
  p13: number;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  // private _baseURL = 'http://10.142.158.62:8082';
  // private _jsonURL1 = 'assets/graphsData.json';
  // private _jsonURL2 = 'assets/hireData.json';
  // private _jsonURL3 = 'assets/rollOffRollOnData.json';
  // private _jsonURL4 = 'assets/graphRollOffData.json';
  // private _jsonURL5 = 'assets/graphAvailability.json';
  // private _jsonURL6 = 'assets/graphResignation.json';
  // graph1Data: GraphData = {xaxisData:["1"],yaxisData:[]};
  
  private _baseURL = environment.baseURL;
  private _category = '';

  graph1YaxisData: GraphModel[];
  graph2YaxisData: GraphModel[];
  graph3YaxisData: GraphModel[];
  table1Data: TabData[];
  table2Data: TabData[];
  table3Data: TabData[];
  filterEnabled = false;
  noOfColumns = 16;
  table1Caption = " | Graphs Data";
  table2Caption = "";
  table3Caption = "Table 2: CBO Roll-ons/Roll-offs (Top 10 Accounts)";
  actionHeader1="Action";
  actionHeader2="Comment";
  actionHeader3="Comment";
  isAction1=true;
  isAction2=false;
  isAction3=false;

  constructor(private dashboardService: DashboardServiceService, private activatedroute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedroute.data.subscribe(data => {
      this._category=data.category;
      this.table2Caption=data.category;
      this.table1Caption=data.category + this.table1Caption;
      console.log('Category -',this._category+' | Table Caption 1',this.table1Caption+' | Table Caption 2',this.table2Caption+' | Table Caption 3',this.table3Caption);
  });
  
  let _jsonURL1 = this._baseURL+'/graphsData?category='+this._category;
  let _jsonURL2 = this._baseURL+'/hireData?category='+this._category;
  let _jsonURL3 = this._baseURL+'/rollOnRollOffData?category='+this._category;
  
  let _jsonURL4 = this._baseURL+'/rollOffGraph?category='+this._category;
  let _jsonURL5 = this._baseURL+'/availabilityGraph?category='+this._category;
  let _jsonURL6 = this._baseURL+'/resignationGraph?category='+this._category;

   
    this.dashboardService.getGraphData(_jsonURL4).then(response => {
      // this.graph1Data = response;
      // console.log("Graph Data - "+JSON.stringify(response));
      this.graph1YaxisData=response.yaxisData;
      // console.log("Graph Y Axis - "+JSON.stringify(this.graph1YaxisData));
    });
    this.dashboardService.getGraphData(_jsonURL5).then(response => {
      // this.graph1Data = response;
      // console.log("Graph Data - "+JSON.stringify(response));
      this.graph2YaxisData=response.yaxisData;
      // console.log("Graph Y Axis - "+JSON.stringify(this.graph2YaxisData));
    });
    this.dashboardService.getGraphData(_jsonURL6).then(response => {
      // this.graph1Data = response;
      // console.log("Graph Data - "+JSON.stringify(response));
      this.graph3YaxisData=response.yaxisData;
      // console.log("Graph Y Axis - "+JSON.stringify(this.graph3YaxisData));
    });
    this.dashboardService.getData(_jsonURL1).then(tableData => this.table1Data = tableData);
    this.dashboardService.getData(_jsonURL2).then(tableData => this.table2Data = tableData);
    this.dashboardService.getData(_jsonURL3).then(tableData => this.table3Data = tableData);
  }
  
   //Column headers for primeNg datatable editable row
  columnHeaders = [
    { field: 'category', header: 'Category' , isEditable: false},
    { field: 'p1', header: 'P1' , isEditable: false},
    { field: 'p2', header: 'P2' , isEditable: false},
    { field: 'p3', header: 'P3' , isEditable: false},
    { field: 'p4', header: 'P4' , isEditable: false},
    { field: 'p5', header: 'P5' , isEditable: false},
    { field: 'p6', header: 'P6' , isEditable: false},
    { field: 'p7', header: 'P7' , isEditable: false},
    { field: 'p8', header: 'P8' , isEditable: false},
    { field: 'p9', header: 'P9' , isEditable: true},
    { field: 'p10', header: 'P10' , isEditable: true},
    { field: 'p11', header: 'P11' , isEditable: true},
    { field: 'p12', header: 'P12' , isEditable: true},
    { field: 'p13', header: 'P13' , isEditable: true}
  ];

  //TABLE 2
    //Column headers for primeNg datatable
    table2ColumnHeaders = [
      { field: 'category', header: 'Category' , isEditable: false},
      { field: 'p1', header: 'P1' , isEditable: false},
      { field: 'p2', header: 'P2' , isEditable: false},
      { field: 'p3', header: 'P3' , isEditable: false},
      { field: 'p4', header: 'P4' , isEditable: false},
      { field: 'p5', header: 'P5' , isEditable: false},
      { field: 'p6', header: 'P6' , isEditable: false},
      { field: 'p7', header: 'P7' , isEditable: false},
      { field: 'p8', header: 'P8' , isEditable: false},
      { field: 'p9', header: 'P9' , isEditable: false},
      { field: 'p10', header: 'P10' , isEditable: false},
      { field: 'p11', header: 'P11' , isEditable: false},
      { field: 'p12', header: 'P12' , isEditable: false},
      { field: 'p13', header: 'P13' , isEditable: false}
    ];

    table3ColumnHeaders = [
      { field: 'account', header: 'Account' , isEditable: false},
      { field: 'requestType', header: 'Request Type' , isEditable: false},
      { field: 'sourceType', header: 'Source Type' , isEditable: false},
      { field: 'probability', header: 'Probability' , isEditable: false},
      { field: 'p1', header: 'P1' , isEditable: false},
      { field: 'p2', header: 'P2' , isEditable: false},
      { field: 'p3', header: 'P3' , isEditable: false},
      { field: 'p4', header: 'P4' , isEditable: false},
      { field: 'p5', header: 'P5' , isEditable: false},
      { field: 'p6', header: 'P6' , isEditable: false},
      { field: 'p7', header: 'P7' , isEditable: false},
      { field: 'p8', header: 'P8' , isEditable: false},
      { field: 'p9', header: 'P9' , isEditable: false},
      { field: 'p10', header: 'P10' , isEditable: false},
      { field: 'p11', header: 'P11' , isEditable: false},
      { field: 'p12', header: 'P12' , isEditable: false},
      { field: 'p13', header: 'P13' , isEditable: false}
    ];









//NOT IN USE 
    tableData: TableElement[] = [
      {category: 'Roll-off Forecast', p1: 0, p2: 0, p3: 0, p4: 0, p5: 0, p6: 0, p7: 0, p8: 0, p9: 70, p10: 50, p11: 40, p12: 30, p13:75},
      {category: 'Roll-off Actuals', p1: 1, p2: 1, p3: 2, p4: 3, p5: 21, p6: 74, p7: 117, p8: 341, p9: 77, p10: 51, p11: 46, p12: 35, p13:79},
      {category: 'Resigned', p1: 10, p2: 0, p3: 0, p4: 0, p5: 0, p6: 0, p7: 0, p8: 0, p9: 0, p10: 0, p11: 0, p12: 0, p13:0},
      {category: 'Allowable Attrition', p1: 0, p2: 0, p3: 0, p4: 0, p5: 0, p6: 0, p7: 0, p8: 0, p9: 0, p10: 0, p11: 0, p12: 0, p13:0},
      {category: 'Availability', p1: 23, p2: 42, p3: 85, p4: 108, p5: 142, p6: 162, p7: 219, p8: 675, p9: 813, p10: 873, p11: 932, p12: 975, p13:1065},
      {category: 'Maximum Bench Strength', p1: 0, p2: 0, p3: 0, p4: 0, p5: 0, p6: 0, p7: 0, p8: 0, p9: 0, p10: 0, p11: 0, p12: 0, p13:0},
      {category: 'Minimum Bench Strength', p1: 0, p2: 0, p3: 0, p4: 0, p5: 0, p6: 0, p7: 0, p8: 0, p9: 0, p10: 0, p11: 0, p12: 0, p13:0}
    ];
  
    //Column Header for Material Datatable
    columnHeader = {'category': 'Category', 'p1': 'P1', 'p2': 'P2', 'p3': 'P3', 'p4': 'P4', 'p5': 'P5', 'p6': 'P6', 'p7': 'P7', 'p8': 'P8', 'p9': 'P9', 'p10': 'P10', 'p11': 'P11', 'p12': 'P12', 'p13': 'P13', 'action':'Action'};
    
  
}
